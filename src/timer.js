function Timer(elementId, maxTime, startCallback, finishCallback, stopCallback) {
    this.maxTime = maxTime;
    this.elementId = elementId;
    this.startCallback = startCallback;
    this.finishCallback = finishCallback;
    this.stopCallback = stopCallback;
    this.finishTimeout = undefined;
    this.starttimestamp = 0;

    this._changeClockEnabledProp = function (enabled) {
        var clockArrowEl = document.getElementById(this.elementId);
        var props = clockArrowEl.getAttribute("animation");
        props.enabled = enabled;
        props.dur = this.maxTime;
        clockArrowEl.setAttribute("animation", props);
    }

    this._stopTimer = function () {
        this._changeClockEnabledProp(false);
        clearTimeout(this.finishTimeout);
    }
}

Timer.prototype.stop = function () {
    this._stopTimer();
    if (this.stopCallback) {
        this.stopCallback((new Date()).getTime() - this.starttimestamp);
    }
}

Timer.prototype.start = function () {
    const self = this;
    this._changeClockEnabledProp(true);
    this.starttimestamp = (new Date()).getTime();
    this.finishTimeout = setTimeout(function () {
        self.stop();
        if (self.finishCallback) {
            self.finishCallback();
        }
    }, self.maxTime);
    if (this.startCallback) {
        this.startCallback();
    }
}

Timer.prototype.reset = function () {
    this.stop();
}