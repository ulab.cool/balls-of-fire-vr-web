// turn controller's physics presence on only while button held down
AFRAME.registerComponent('phase-shift', {
    init: function () {
        const el = this.el
        el.addEventListener('gripdown', function () {
            el.setAttribute('collision-filter', { collisionForces: true })
        })
        el.addEventListener('gripup', function () {
            el.setAttribute('collision-filter', { collisionForces: false })
        })
    }
});

AFRAME.registerComponent('box-counter', {
    init: function () {
        const el = this.el;
        el.addEventListener('hitstart', function (evt) {
            window.game.countBallsForBox(el);
        });
        el.addEventListener('hitend', function (evt) {
            window.game.countBallsForBox(el);
        });
    }
});
