
var maxTime = 2 * 60 * 1000; // 2 minutes
var timer = undefined;

function Game() {
    this.totalBalls = 20;
    this.availableTypeBalls = ["ub", "svmk"];
    this.balls = [];
    this.boxes = {
        ub: 0,
        svmk: 0,
    };
    this.started = false;
    this.gameOver = false;
    this.startButtonEl = document.getElementById('start-button');
    this.resetButtonEl = document.getElementById('reset-button');

    this.startButtonEl.addEventListener('hover-start', this.start.bind(this));
    this.resetButtonEl.addEventListener('hover-start', this.reset.bind(this));
}

Game.prototype.reset = function () {
    if (!this.started) {
        return;
    }
    this.balls.forEach(function (ball) {
        ball.remove();
    });
    this.balls = [];
    this.started = false;
    this.gameOver = false;
    self.timer.reset();
    this.checkWin();
}

Game.prototype.start = function () {
    const self = this;
    if (this.started) {
        return;
    }
    this.started = true;
    this.gameOver = false;
    this.generateBalls(function () {
        //wait for the balls and then start the timer
        self.timer = new Timer("animatedClockArrow", maxTime, function () {
            console.log("timer started");
        }, function () {
            self.gameOver = 'loose';
            self.checkWin();
        }, function (time) {
            var minutes = Math.floor(time / 1000 / 60);
            var seconds = time % 60;
            self.display("Game completed in " + minutes + " min. and " + seconds + " sec.");
        });
        self.timer.start();
    });
}

Game.prototype.generateBalls = function (generatedCallback) {
    const self = this;
    for (var i = 0; i < this.totalBalls; i++) {
        this.balls.push(new Ball(this.availableTypeBalls[i % 2]));
    }
    shuffleArray(this.balls);
    this.balls.forEach(function (ball, i) {
        setTimeout(function () {
            ball.init(i % 2 == 0);
            if (i === self.totalBalls - 1 && generatedCallback) {
                generatedCallback();
            }
        }, 100 + 250 * (i * 2 + 1))
    });
}

Game.prototype.countBallsForBox = function (box) {
    const type = box.getAttribute('type');
    const count = box.components['aabb-collider'].intersectedEls.filter(function (ball) {
        return ball.getAttribute('type') == type;
    }).length;

    this.boxes[type] = count;

    this.checkWin();
}

Game.prototype.checkWin = function () {
    if (this.gameOver) {
        this.display("Game over! You " + this.gameOver + " You have got " + this.boxes.ub + " UB  and " + this.boxes.svmk + " SVMK balls");
        return;
    }
    if (!this.started) {
        this.display('Press the start button');
        return;
    }
    var totalGoodBalls = 0;
    for (var type in this.boxes) {
        totalGoodBalls += this.boxes[type];
    }

    if (totalGoodBalls == this.totalBalls) {
        this.gameOver = 'win';
        this.timer.stop();
        this.checkWin();
    } else {
        this.display('UB: ' + this.boxes.ub + ' - SVMK:' + this.boxes.svmk);
    }
}

Game.prototype.display = function (text) {
    const logger = document.getElementById('log');
    logger.setAttribute('text', { value: text });
}

// Utils
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

function getRandom0to10() {
    return Math.floor(Math.random() * 10);
}

// Ball
function Ball(type) {
    this.el = null;
    this.type = type;
}

Ball.prototype.init = function (isRightBox) {
    this.create();
    this.positionateBallForDrop(isRightBox);
    this.addToDom();
}

Ball.prototype.create = function () {
    this.el = document.createElement('a-sphere');
    this.el.setAttribute('type', this.type);
    this.el.setAttribute("mixin", "ball");
    this.el.setAttribute("class", "ball");
    this.el.setAttribute("src", this.type == 'ub' ? '#ballUbTexture' : '#ballSvmkTexture')
}

Ball.prototype.positionateBallForDrop = function (isRightBox) {
    var xPos = 0.27 + getRandom0to10() / 100;
    if (isRightBox) {
        xPos = xPos * -1;
    }
    this.el.setAttribute("position", xPos + " 2 -0.3" + getRandom0to10());
}

Ball.prototype.remove = function () {
    this.el.remove();
}

Ball.prototype.addToDom = function () {
    document.getElementById("game").appendChild(this.el);
}

window.game = new Game();
