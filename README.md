# Balls Of Fire VR
This project is VR version of the infamous Usabilla Balls of Fire Game. The game can be played via compatible VR browsers these include the Oculus Quest, Oculus Rift, or Rift S (or any device compatible with the oculus-touch-controls A-Frame component). 

# Objective of the Game
The player need to, as quickly as possible, move all the balls to the correct ball box. As soon as all balls are in the correct box the time will start. If you do not have all balls in the correct box your score is determined by the number of correctly place balls.

# How to play the Game
Playing the game is as easy as the real-world version of the game. Simpyly strap on your VR headset and press the big red button on the table in front of you. After clicking on the button you'll see the playing balls drop into the bins. After the balls have dropped you need to grab them and put them in the correct box.

# Tips
* Be carefull when playing in VR, make sure your environment is clear of any obstacles
* While the table may look real, it's not! So do not try to lean on it because it will hurt 😉

# Technology
* A-frame
* A-frame Super Hands
* A-frame Physics
* A-frame Extras